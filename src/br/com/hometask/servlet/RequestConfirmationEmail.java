package br.com.hometask.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;
import br.com.hometask.person.verification.CadastreAlreadyActive;
import br.com.hometask.person.verification.CadastreConfirmationController;
import br.com.hometask.person.verification.ICadastreConfirmationController;

/**
 * Servlet implementation class RequestConfirmationEmail
 */
@WebServlet("/RequestConfirmationEmail")
public class RequestConfirmationEmail extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private PersonController personController;
	
	private ICadastreConfirmationController cadastreConfirmationController; 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RequestConfirmationEmail() {
        super();
        
        personController = new PersonController();
        
        cadastreConfirmationController = new CadastreConfirmationController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		
		Person person = personController.getPerson(email);
		
		if(person != null){
			
			try {
				cadastreConfirmationController.createVerficationToken(person);
				
			} catch (CadastreAlreadyActive e) {
				
				
			}finally{
				
				response.sendRedirect("admin/");
			}
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

}
