package br.com.hometask.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.hometask.DataValidator;
import br.com.hometask.HashSHA512;
import br.com.hometask.IDataValidator;
import br.com.hometask.email.Email;
import br.com.hometask.email.EmailController;
import br.com.hometask.person.IPersonController;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;
import br.com.hometask.person.verification.CadastreAlreadyActive;
import br.com.hometask.person.verification.CadastreConfirmationController;
import br.com.hometask.person.verification.ICadastreConfirmationController;

/**
 * Servlet implementation class Recover Pass
 */
@WebServlet(name="Recover Pass", urlPatterns={"/recover"})
public class RecoverPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	IPersonController personController;
	EmailController emailController;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecoverPassword() {
        super();
        // TODO Auto-generated constructor stub
        this.personController = new PersonController();
        this.emailController = new EmailController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long time = new Date().getTime();
		
		String senha = new HashSHA512().get_SHA_512_SecurePassword(time+"");
		
		String email = request.getParameter("email");
		Person p = new Person();
		p.setEmail(email);
		p.setPassword(senha);
		
		personController.updatePassword(p);
		
		Email emailc = new Email();
		emailc.setRecipient(email);
		emailc.setSender("italosestilon@gmail.com");
		emailc.setText("Sua nova senha é: "+time);
		emailc.setSubject("Nova Senha");
		
		
		emailController.send(emailc);
		
		request.setAttribute("confirmation", "Verifique sua caixa de email para mais informações");
		request.getRequestDispatcher("recover_password.jsp").forward(request, response);
	}

}
