package br.com.hometask.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.hometask.person.Person;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter(urlPatterns = { "/admin/*", "/addGroup", "/addMember", "/addTask",
		"/deactiveAccount", "/editGroup", "/editTask", "/listGroups",
		"/listMembers", "/listTasks", "/removeGroup", "/logout",
		"/removeMember", "/removeTask", "/UpdatePersonalInformation", "/deactivate", "/updateDoneTask"})
public class LoginFilter implements Filter {

	public LoginFilter() {

	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession(false);

		if (session == null) {
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			httpResponse.sendRedirect("/HomeTask2/login.jsp");
			return;
		}

		Person person = (Person) session.getAttribute("person");

		if (person == null) {
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			httpResponse.sendRedirect("/HomeTask2/login.jsp");
			return;
		}

		chain.doFilter(request, response);
		return;

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
