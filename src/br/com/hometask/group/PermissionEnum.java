package br.com.hometask.group;

public enum PermissionEnum {
	
	REMOVE_MEMBER(1);
	
	
	int value;
	
	private PermissionEnum(int value) {
		this.value = value;
	}
	
	public int getValor(){
		return this.value;
	}
}
