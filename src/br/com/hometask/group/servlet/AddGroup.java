package br.com.hometask.group.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import br.com.hometask.DataValidator;
import br.com.hometask.IDataValidator;
import br.com.hometask.group.Group;
import br.com.hometask.group.GroupController;
import br.com.hometask.group.IGroupController;
import br.com.hometask.person.Person;

/**
 * Servlet implementation class AddGroup
 */
@WebServlet(name="Add Group", urlPatterns={"/addGroup"})
public class AddGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	private IGroupController groupController;
	
	private IDataValidator dataValidator;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddGroup() {
        super();
        
        groupController = new GroupController();
        dataValidator = new DataValidator();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession(false);
		
		ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
		String json = "error";
			
		String name = request.getParameter("name");

		Group group = new Group();
		group.setName(name);
			
		Person person = (Person)session.getAttribute("person");
	
		if(dataValidator.isGroupInformationValid(group)){
					
			group = groupController.addGroup(group, person);
				
			json = objectWriter.writeValueAsString(group);
		}
		out.write(json);	
	}
}
