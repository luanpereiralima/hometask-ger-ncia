package br.com.hometask.group.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import br.com.hometask.group.Group;
import br.com.hometask.group.GroupController;
import br.com.hometask.group.IGroupController;
import br.com.hometask.person.Person;

/**
 * Servlet implementation class ListMembersGroup
 */
@WebServlet("/listMembers")
public class ListMembersGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IGroupController groupController;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListMembersGroup() {
        super();
       
        groupController = new GroupController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
		try{
			Long id = Long.parseUnsignedLong(request.getParameter("idGroup"));
			Group group = groupController.getGroupById(id);
			String json = "error";
			if(group != null){
				json = objectWriter.writeValueAsString(group.getMembers());
			}
			
			response.getWriter().write(json);
		}catch(RuntimeException e){
			
		}
	}

}
