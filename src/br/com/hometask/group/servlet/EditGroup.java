package br.com.hometask.group.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import br.com.hometask.DataValidator;
import br.com.hometask.IDataValidator;
import br.com.hometask.group.Group;
import br.com.hometask.group.GroupController;
import br.com.hometask.group.IGroupController;
import br.com.hometask.person.Person;

/**
 * Servlet implementation class EditGruop
 */
@WebServlet("/editGroup")
public class EditGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IGroupController groupController;
	
	private IDataValidator dataValidator;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditGroup() {
        super();
        
        groupController = new GroupController();
        
        dataValidator = new DataValidator();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		
		ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
		String json = "error";
				
		String name = request.getParameter("name");
		String id = request.getParameter("idGroup");
			
		Group group = new Group();
			
		group.setName(name);
			
		Person person = (Person) session.getAttribute("person");
			
		if(dataValidator.isGroupInformationValid(group)){
					
			try{
				long idGroup = Long.parseLong(id);
						
				group.setId(idGroup);
				groupController.updateGroup(group);
				json = objectWriter.writeValueAsString(group);
						
			}catch(RuntimeException e){
				
			}
		}
			
		response.getWriter().write(json);
	}

}
