package br.com.hometask.group;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.hometask.ConnectionFactory;
import br.com.hometask.IConnectionFactory;
import br.com.hometask.person.Person;
import br.com.hometask.task.ITaskController;
import br.com.hometask.task.Task;
import br.com.hometask.task.TaskController;

public class GroupMemberDAO implements IGroupMemberDAO{

	IConnectionFactory factory;
	
	public GroupMemberDAO() {
		factory = new ConnectionFactory();
	}
	
	@Override
	public void insert(Group group, Person newMember) {
		
		Connection connection = factory.getConnection();
		
		String query = "insert into group_member (member_id,group_id) values (?,?)";
		
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			
			statement.setLong(1, newMember.getId());
			statement.setLong(2, group.getId());
			
			statement.execute();
			statement.close();
	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		} finally{
			
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean remove(Group group, Person member) {
		
		Connection connection = this.factory.getConnection();
		
		ITaskController taskController = new TaskController();
		List<Task> taskList = taskController.getAllTaskOfAGroup(group);
		
		String query = "delete from group_member where group_id = ? AND member_id = ?";
		String query2 = "delete from responsability where task_id = ? AND person_id = ?";
		
		boolean result;	
		try {
			
			PreparedStatement statement2 = connection.prepareStatement(query2);
						
			for(Task task : taskList){
				statement2.setLong(1, task.getId());
				statement2.setLong(2, member.getId());
				statement2.executeUpdate();
			}
			 
			PreparedStatement statement = connection.prepareStatement(query);
			
			statement.setLong(1, group.getId());
			statement.setLong(2, member.getId());
			
			result = statement.executeUpdate() > 0;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally{
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<Long> getAllByMember(Person member) {
		
		Connection connection = this.factory.getConnection();
		
		String query = "select group_id from group_member where member_id = ?";
		
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			
			statement.setLong(1,member.getId());
			
			ResultSet result = statement.executeQuery();
			
			List<Long> groupIDs = new ArrayList<Long>();
			
			while(result != null && result.next()){
				groupIDs.add(result.getLong("group_id"));
			}
			
			return groupIDs;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	@Override
	public List<Long> getAllMembers(Group group) {
		
		Connection connection = this.factory.getConnection();
		
		String query = "select member_id from group_member where group_id = ?";
		
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			
			statement.setLong(1, group.getId());
			
			ResultSet result = statement.executeQuery();
			
			List<Long> membersIDs = new ArrayList<Long>();
			
			while(result != null && result.next()){
				membersIDs.add(result.getLong("member_id"));
			}
			
			return membersIDs;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	

}
