package br.com.hometask.person;

public interface IPersonController {

	public void addPerson(Person person);
	public boolean doesEmailAlreadyExists(String email);
	public Person getPerson(String email);
	public void updatePerson(Person person);
	public void updateImage(Person person);
	public void updatePassword(Person person);
	public Person getOneById(long id);
	void activePerson(Person person);
}
