package br.com.hometask.person.verification;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class DeleteVerificationTokenJob implements Job{
	
	private ICadastreConfirmationController cadastreConfirmationController;
	
	public DeleteVerificationTokenJob() {
		
		cadastreConfirmationController = new CadastreConfirmationController();
	}
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		
		VerificationToken token = (VerificationToken) dataMap.get("token");
		
		cadastreConfirmationController.removerVerificationToken(token);
		
	}

}
