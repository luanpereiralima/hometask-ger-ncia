package br.com.hometask.task.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import br.com.hometask.DataValidator;
import br.com.hometask.IDataValidator;
import br.com.hometask.group.Group;
import br.com.hometask.group.GroupController;
import br.com.hometask.group.IGroupController;
import br.com.hometask.person.IPersonController;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;
import br.com.hometask.task.ITaskController;
import br.com.hometask.task.Task;
import br.com.hometask.task.TaskController;

/**
 * Servlet implementation class AddTask
 */
@WebServlet("/addTask")
public class AddTask extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ITaskController taskController;

	private IPersonController personController;

	private IDataValidator dataValidador;
	
	private IGroupController groupController;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddTask() {
		super();

		this.taskController = new TaskController();
		this.dataValidador = new DataValidator();
		this.personController = new PersonController();
		this.groupController = new GroupController();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

	/**
	 * @param reponsible
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String name = request.getParameter("name");
		String description = request.getParameter("description");
		String responsibles = request.getParameter("responsibles");
		String dateTask = request.getParameter("date");
		String timeTask = request.getParameter("time");
		String idGroupParam = request.getParameter("idGroup");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = null;

		try {
			if(dateTask != null && !dateTask.equals("") && timeTask != null && !timeTask.equals("")){
				date = dateFormat.parse(dateTask+" "+timeTask);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
		
		long idGroup = 0;
		
		try{
			
			idGroup = Long.parseLong(idGroupParam);
			
		}catch(NumberFormatException e){
			
			e.printStackTrace();
		}
		
		
		
		Task task = new Task();

		task.setName(name);
		task.setDescription(description.trim());
		task.setDate(date);
		
		Group group = groupController.getGroupById(idGroup);
		
		if(group != null){
			
			task.setGroup(group);
		
		}


		if (responsibles != null) {
			for (String responsible : responsibles.split(",")) {
				if (!responsible.equals("")) {
					Person person = personController.getPerson(responsible);

					if (person != null) {
						task.addResponsible(person);
					}
				}
			}
		}

		String json = "error";

		if (dataValidador.isTaskInformationValid(task)) {

			ObjectWriter objectWriter = new ObjectMapper().writer()
					.withDefaultPrettyPrinter();
			
			task = taskController.addTask(task);
			json = objectWriter.withDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm")).writeValueAsString(task);
			System.out.println(json);
		}

		response.getWriter().write(json);
	}
}
