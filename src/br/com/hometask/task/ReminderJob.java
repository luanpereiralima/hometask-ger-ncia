package br.com.hometask.task;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.hometask.email.Email;
import br.com.hometask.email.EmailController;
import br.com.hometask.person.Person;

public class ReminderJob implements Job{
	
	private ITaskDAO taskDAO;
	
	private EmailController emailController;
	
	public ReminderJob() {
		taskDAO = new TaskDAO();
		emailController = new EmailController();
	}
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		
		long taskId= (long) dataMap.get("task_id");
		
		Task task = taskDAO.getOne(taskId);
		
		for(Person person : task.getResponsibles()){
			
			Email email = new Email();
			
			email.setSubject("Lembreta da tarefa "+task.getName());
			email.setSender("italosestilon@hotmail.com");
			email.setRecipient(person.getEmail());
			email.setText("Lembre de fazer a tarefa"+task.getDescription());
			
			emailController.send(email);
		}
		
		
		taskDAO.update(task);
		
	}

}
