package br.com.hometask.email;


import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailController {
	
	private final String userName = "lancelot12334@hotmail.com";
	
	private final String password = "senha123";
	
	private final String host = "smtp.live.com";
	
	private final String port = "587";
	
	private final String enable = "true";
	
	private final String auth = "true";
	
	private Session session;
	
	
	public EmailController(){
		
		authenticate();
	}
	
	public void send(Email email){
		
		try {
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(email.getSender()));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email.getRecipient()));
			message.setSubject(email.getSubject());
			message.setText(email.getText());
			
			Transport.send(message);
			
		} catch (MessagingException e) {
			
			e.printStackTrace();
		}catch (IllegalStateException e) {
			System.out.println("Estado ilegal.");
		}
	}
	
	private void authenticate(){
	
		Properties props = new Properties();
		
		props.put("mail.smtp.auth", auth);
		props.put("mail.smtp.starttls.enable", enable);
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		
		session = Session.getDefaultInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication(){
				return new PasswordAuthentication(userName, password);
			}
		});
	}
}
