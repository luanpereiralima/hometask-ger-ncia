package test.person;

import org.junit.Assert;
import org.junit.Test;

import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;

public class PersonTest {

	
	@Test
	public void testAddNewPerson(){
		
		PersonController controller = new PersonController();
		
		Person person = new Person();
		
		person.setEmail("i@i.com");
		person.setName("Name");
		person.setPassword("Senha");
		person.setProfilePicturePATH("path_image");
		
		controller.addPerson(person);
		
		Assert.assertNotNull("falha - pessoas não adicionada",
				controller.getPerson(person.getEmail()));
		
		Assert.assertEquals("falha - pessoa adicioanda não adicionada"
				,person.getEmail()
				,controller.getPerson(person.getEmail()).getEmail());
		
		
	}
	
	@Test
	public void testUpdatePerson(){
		
		PersonController controller = new PersonController();
		
		Person person = new Person();
		
		person.setEmail("iii@i.com");
		person.setName("Name");
		person.setPassword("Senha");
		person.setProfilePicturePATH("path_image");
		
		controller.addPerson(person);
		
		person = controller.getPerson(person.getEmail());
		
		person.setName("Novo nome");
		person.setEmail("novo_email@i.com");
		person.setPassword("Nova senha");
		
		controller.updatePerson(person);
		
		Person updatedPerson = controller.getPerson(person.getEmail());
		
		Assert.assertEquals("falha - email não está igual"
				,person.getEmail()
				,updatedPerson.getEmail());
		Assert.assertEquals("falha - senha não está igual"
				,person.getPassword()
				,updatedPerson.getPassword());
		Assert.assertEquals("falha - nome não está igual"
				,person.getName()
				,updatedPerson.getName());
		Assert.assertEquals("falha - id está diferente"
				,person.getId()
				,updatedPerson.getId());
	}
	
	@Test
	public void testEmailAlreadyExist(){
		
		PersonController controller = new PersonController();
		
		Person person = new Person();
		
		person.setEmail("i@i.com");
		person.setName("Name");
		person.setPassword("Senha");
		person.setProfilePicturePATH("path_image");
		
		controller.addPerson(person);
		
		Assert.assertEquals("falha - email não existe",
				true,
				controller.doesEmailAlreadyExists(person.getEmail()));
	}
	
	@Test
	public void testGetPersonById(){
		PersonController controller = new PersonController();
		
		Person person = new Person();
		
		person.setEmail("i@i.com");
		person.setName("Name");
		person.setPassword("Senha");
		person.setProfilePicturePATH("path_image");
		
		controller.addPerson(person);
		
		person = controller.getPerson(person.getEmail());
		
		person = controller.getOneById(person.getId());
		
		Assert.assertEquals("falha - email não está igual"
				,person.getEmail()
				,person.getEmail());
		Assert.assertEquals("falha - senha não está igual"
				,person.getPassword()
				,person.getPassword());
		Assert.assertEquals("falha - nome não está igual"
				,person.getName()
				,person.getName());
		Assert.assertEquals("falha - id está diferente"
				,person.getId()
				,person.getId());
		
	}
	
	@Test
	public void testChanheActive(){
		
PersonController controller = new PersonController();
		
		Person person = new Person();
		
		person.setEmail("ier@i.com");
		person.setName("Name");
		person.setPassword("Senha");
		person.setProfilePicturePATH("path_image");
		
		controller.addPerson(person);
		
		person = controller.getPerson(person.getEmail());
		
		controller.activePerson(person);
		
		person = controller.getPerson(person.getEmail());
		
		Assert.assertEquals("falha - pessoa não está ativa", true, person.isActive());
		
	}
	
}
