/**
 * 
 */
$(document).ready(function () {
	 $("#date-popover").popover({html: true, trigger: "manual"});
     $("#date-popover").hide();
     $("#date-popover").click(function (e) {
         $(this).hide();
     });
     
     TaskList.initTaskWidget();
     
     $( "#sortable" ).sortable();
     $( "#sortable" ).disableSelection();
     
     autosize($('* .resize'));
 });
 
 
 function myNavFunction(id) {
     $("#date-popover").hide();
     var nav = $("#" + id).data("navigation");
     var to = $("#" + id).data("to");
     console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
 }        