var isValidatedName = false;
var isvalidatedDescription = false;
var isValidatedTime = false;
var isValidatedDate = false;

var validateName = function(name){
	if(name != null && name.trim().length > 0){
		$("#taskName-form-group").addClass("has-success").removeClass("has-error");
		$("#taskName-error-message").html("");
		isValidatedName = true;
	}else{
		$("#taskName-form-group").addClass("has-error").removeClass("has-success");
		$("#taskName-error-message").html("O nome n&atilde;o pode ser vazio!");
		isValidatedName = false;
	}
}

var validateDescription = function(description){
	if(description != null && description.trim().length > 0){
		$("#taskDescription-form-group").addClass("has-success").removeClass("has-error");
		$("#taskDescription-error-message").html("");
		isValidatedName = true;
	}else{
		$("#taskDescription-form-group").addClass("has-error").removeClass("has-success");
		$("#taskDescription-error-message").html("A descri&ccdeil;&atilde;o n&atilde;o pode ser vazia!");
		isValidatedName = false;
	}
}

var validateTime = function(time){
	var regex = /^([01]\d|2[0-3]):?([0-5]\d)$/;
	if(time.trim().length > 0 && regex.test(time)){
		$("#taskTime-form-group").addClass("has-success").removeClass("has-error");
		$("#taskTime-error-message").html("");
		isValidatedTime = true;
	}else{
		$("#taskTime-form-group").addClass("has-error").removeClass("has-success");
		$("#taskTime-error-message").html("O tempo deve ser preenchido e deve estar no formato HH:MM");
		isValidatedTime = false;
	}
	
}

var validateDate = function(date){
	if(date != null && date.trim().length > 0){
		$("#taskDate-form-group").addClass("has-success").removeClass("has-error");
		$("#taskDate-error-message").html("");
		isValidatedDate = true;
	}else{
		$("#taskDate-form-group").addClass("has-error").removeClass("has-success");
		$("#taskDate-error-message").html("A data deve ser preenchida!");
		isValidatedDate = false;
	}
}

$(document).ready(function(){
	
	$("#taskName", "#addTaskForm").keyup(function(){
		var name = $(this).val();
		validateName(name);
	}).blur(function(){
		var name = $(this).val();
		validateName(name);
	});
	
	$("#taskDescription", "#addTaskForm").keyup(function(){
		var description = $(this).val();
		validateDescription(description);
	}).blur(function(){
		var description = $(this).val();
		validateDescription(description);
	});
	
	$("#taskTime", "#addTaskForm").keyup(function(){
		var time = $(this).val();
		validateTime(time);
	}).blur(function(){
		var time = $(this).val();
		validateTime(time);
	});
	
	$("#taskDate", "#addTaskForm").keyup(function(){
		var date = $(this).val();
		validateDate(date);
	}).blur(function(){
		var date = $(this).val();
		validateDate(date);
	});
});