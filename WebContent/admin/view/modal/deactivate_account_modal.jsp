<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<div class="modal fade" id="deactivate_account_modal" tabindex="-1"
	role="dialog" aria-labelledby="deactivate_account_modal"
	aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="addTaskMotalLabel">Desativar Perfi</h4>
				<h5 class="modal-title">
					Deseja realmente desativar seu perfil? <br /> Digite sua senha
					para confirmar:<br />

				</h5>
			</div>
			<form id="deactivate_account_from" class="form-horizontal "
				method="post">
				<div class="modal-body">

					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label">Senha:</label>
						<div class="col-sm-9">
							<input type="password" id="deactivate_account_modal_password"
								class="form-control" value="">
							<div class='display-none' id="error-message">Senha inválida!</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<input type="submit" value="Desativar Perfil" class="btn btn-ht">
				</div>
			</form>
		</div>
	</div>
</div>