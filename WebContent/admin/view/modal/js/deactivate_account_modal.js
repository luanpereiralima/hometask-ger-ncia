$(document).ready(function(){
		
		$("#deactivate_account_from").submit(function(){
				
				var password = $("#deactivate_account_modal_password").val();
				
				$.ajax({
					url:"../deactivate",
					type:"post",
					data:{password:password},
					beforeSend:function(){
					},
					success:function(retorno){
						if(retorno == "true"){
							window.location = "../admin/index.html";
						}else{
							$("#error-message", "#deactivate_account_from").show();
						}
					}
				});
				
				return false;
			
			});
		});