/**
 * 
 */

var addMember = function(member){
		var newMember = "<div class='desc' id='member"+member.id+"'>" +
							"<div class='thumb'>"+
                      			"<img class='img-circle' src='"+member.profilePicturePATH+"' width='35px' height='35px' align=''>"+
                      		"</div>" +
                      		"<i title='Remover' id='removeMember"+member.id+"' class='fa fa-trash-o btn remove'></i>" +
                      		"<div class='details'>" +
                      			"<p>" +
                      				"<a href='#'>"+member.name+"</a>" +
                      				"<br />" +
                      				"<muted>"+member.email+"</muted>"+
                      			"</p>" +
                      		"</div>" +
                      	"</div>";
		
		
		$("#membersList").append(newMember);
		
		$("#removeMember"+member.id).click(function(){
			bootbox.confirm("Deseja mesmo excluir esse membro?", function(flag){
				if(flag){
					if(idGroup != null){
						$.ajax({
							url:"../removeMember",
							type:"post",
							data:{memberID: member.id, groupID:idGroup},
							success:function(response){
								if(response == "false-permission"){
									alert("Você não possui permissão para remover esse membro!");
								}else if(response == "false-error"){
									alert("Ocorreu algum erro!");
								}else{
									$("#member"+member.id).remove();
									if(response == "true-leaveGroup"){
										$("#group"+idGroup).closest("li").remove();
										loadGeneralSession();
									}
								}
							}
						});
					}
				}
			});
		});
		
		$("#addTaskCategory").modal('hide');
		$("#nameTaskCategory").val("");
}

var loadMembers = function(idGroup){
	$("#membersList").fadeIn();
	
	$.ajax({
		url:"../listMembers",
		type:"post",
		dataType:"json",
		data:{idGroup:idGroup},
		success:function(response){
			if(response == "error"){
				
			}else{
				for(var i=0; i<response.length; i++){
					addMember(response[i]);
				}
			}
		}
	});
}

var validateEmail = function(email){
	var regexp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(email == null || email.trim().lenght == 0 || !regexp.test(email)){
		return false;
	}
	return true;
}


$(document).ready(function(){
	$("#addMemberForm").submit(function(){
		if(idGroup != null){
			var email = $("#memberEmail", this).val();
			if(validateEmail(email)){
				$.ajax({
					url:"../addMember",
					data:{email: email, idGroup:idGroup},
					type:"post",
					dataType:"json",
					success:function(response){
						if(response == "isMember"){
							$("#email-member-error").html("Esse membro já faz parte do grupo!");
						}else if(response == "error"){
							$("#email-member-error").html("Ocorreu algum erro interno!");
						}else if(response == "memberNotExists"){
							$("#email-member-error").html("Esse usuário não está cadastrado!");
						}else{
							addMember(response);
						}
					},
					error:function(error){
						if(error.responseText == "isMember"){
							$("#email-member-error").html("Esse membro já faz parte do grupo!");
						}else if(error.responseText == "error"){
							$("#email-member-error").html("Ocorreu algum erro interno!");
						}else if(error.responseText == "memberNotExists"){
							$("#email-member-error").html("Esse usuário não está cadastrado!");
						}
					}
				});
			}else{
				$("#email-member-error").html("Informe um email no formato válido!");
			}
		}else{
			$("#email-member-error").html("Você não está em nenhum grupo!");
		}
		return false;
	});
});