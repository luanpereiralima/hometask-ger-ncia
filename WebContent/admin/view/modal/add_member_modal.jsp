<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="modal fade" id="addGroupMember" tabindex="-1" role="dialog" aria-labelledby="addGroupMember" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="addMemberLabel">Adicionar Membro ao Grupo:</h4>
            </div>
			<form id="addMemberForm" class="form-horizontal" method="post" action="../addMember">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label">Email:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="memberEmail" name="email" value="">
							<span class="help-block" id="email-member-error"></span>
						</div>
					</div>
				</div>
				<input type="hidden" name="idGroup" id="idGroup"/>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<input type="submit" value="Adicionar" class="btn btn-ht">
				</div>
			</form>
		</div>
	</div>
</div> 