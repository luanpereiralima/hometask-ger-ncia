<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--sidebar start-->
<aside>
	<div id="sidebar" class="nav-collapse ">
		<!-- sidebar menu start-->
		<ul class="sidebar-menu" id="nav-accordion">

			<p class="centered">
				<a href="#"><img src="${person.profilePicturePATH}"
					class="img-circle" width="60"></a>
			</p>
			<h5 class="centered">${person.name}</h5>

			<li class="mt"><a class="active" id="general" href="#"> <i
					class="fa fa-dashboard"></i> <span>Geral</span>
			</a></li>
			<li class="sub-menu"><i title="Adicionar Grupo"
				class="fa fa-plus btn add" data-toggle="modal"
				data-target="#addGroup"></i> <a id="addGroup" href=""> <i class="fa fa-users"></i>
					<span>Meus Grupos</span>
			</a>
				<ul id="groupList" class="sub">
					<c:if test="${not empty groups}">
						<c:forEach var="group" items="${groups }">
							<li><a href="group?idGroup=${group.id}"> <span>${group.name}</span> <i data-target="${group.id}" title="Editar"
							class="fa fa-edit btn edit"></i> <i data-target="${group.id}" title="Remover"
							class="fa fa-trash-o btn remove"></i>
							</a></li>
						</c:forEach>
					</c:if>
				</ul></li>

			<li class="sub-menu"><a id="config" href=""> <i class="fa fa-cogs"></i>
					<span>Configurações</span></a>
				<ul class="sub">
					<li><a data-toggle="modal" data-target="#deactivate_account_modal"
						href="#">Desativar Perfil</a></li>
					<li><a href="UpdatePersonalInformationForm.jsp">Alterar
							Perfil</a></li>
					<li><a href="addPhoto.jsp">Alterar Foto de 
							Perfil</a></li>
				</ul>
			</li>
		</ul>
		<!-- sidebar menu end-->
	</div>
</aside>
<!--sidebar end-->