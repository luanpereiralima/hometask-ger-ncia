<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

		
		
		<section class="wrapper site-min-height">
				<div class="row mt">
					<div class="col-lg-12">
						<!-- Add Task -->
						<a title="Adicionar nova tarefa" href="#" id="addTaskBtn"
							class="btn display-none" data-toggle="modal" data-target="#addTaskModal"><i
							class="fa fa-plus"></i></a>
						<div class="modal fade" id="addTaskModal" tabindex="-1"
							role="dialog" aria-labelledby="addTaskModalLabel"
							aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-hidden="true">&times;</button>
										<h4 class="modal-title" id="addTaskMotalLabel">Adicionar
											nova tarefa</h4>
									</div>
								
									<form id="addTaskForm" class="form-horizontal" method="post" action="../addTask">
										<div class="modal-body">
											<div class="form-group" id="taskName-form-group">
												<label class="col-sm-3 col-sm-3 control-label">Título
													da tarefa:</label>
												<div class="col-sm-9">
													<input type="text" class="task-form-control form-control" id="taskName" name="name">
													<span class="help-block" id="taskName-error-message"></span>
												</div>
											</div>
											<div class="form-group" id="taskDescription-form-group">
												<label class="col-sm-3 col-sm-3 control-label">Descrição:</label>
												<div class="col-sm-9">
													<textarea class="task-form-control form-control resize"
														name="taskDescription" id="taskDescription"></textarea>
														<span class="help-block" id="taskDescription-error-message"></span>
												</div>
											</div>
											<div class="form-group" id="taskDate-form-group">
												<label class="col-sm-3 col-sm-3 control-label">Data:</label>
												<div class="col-sm-9">
													<input type="date" class="task-form-control form-control" id="taskDate" name="date">
													<span class="help-block" id="taskDate-error-message"></span>
												</div>
											</div>
											<div class="form-group" id="taskTime-form-group">
												<label class="col-sm-3 col-sm-3 control-label">Horário:</label>
												<div class="col-sm-9">
													<input type="text" class="task-form-control form-control" id="taskTime" name="time" placeholder="Ex.: 23:40">
													<span class="help-block" id="taskTime-error-message"></span>
												</div>
											</div>
											<div class="form-group">
												<section class="task-panel tasks-widget">
													<div class="panel-heading">
														<div class="pull-left">
															<h5>
																<i class="fa fa-user"></i> Responsáveis pela tarefa:
															</h5>
														</div>
														<br>
													</div>
													<div class="panel-body">
														<div class="task-content">
															<ul id="sortable" class="task-list">
																
																
															</ul>
																<div class="col-sm-10">
																	<select class="task-form-control form-control" id="selectMembers" name="addedToTask">
																		
																	</select>
																</div>
																<div class="col-sm-2">
																	<input type="button" id="add" value="add" class="btn-ht btn">
																</div>
														</div>
													</div>
												</section>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal" id="close">Fechar</button>
											<input type="submit" value="Adicionar" class="btn btn-ht">
										</div>
									</form>
								</div>
							</div>
						</div>
						<!-- Gridster -->
						<div class="gridster site-min-height">
							<ul id="taskGridster"></ul>
						</div>
						<div id="editTaskModals">
						</div>
						<div id="descriptionsZoom">
						</div>
					</div>
				</div>	

			</section>