<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

 <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div id="toggle-menu" class="fa fa-bars tooltips" data-placement="right" data-original-title="Esconder/Exibir menu"></div>
              </div>
            <!--logo start-->
            <a href="index.jsp" class="logo" id="groupName"><b>HomeTask</b> | GERAL</a>
            <!--logo end-->
            <div class="top-menu pull-right">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="../logout">Sair</a></li>
            	</ul>
            </div>
      </header>
<!--header end-->