<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Dashboard">
<meta name="keyword"
	content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

<title>HomeTask | GERAL</title>

<!-- Bootstrap core CSS -->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<!--external css-->
<link href="../assets/font-awesome/css/font-awesome.css"
	rel="stylesheet" />
<link rel="stylesheet" type="text/css"
	href="../assets/css/zabuto_calendar.css">
<link rel="stylesheet" type="text/css"
	href="../assets/js/gritter/css/jquery.gritter.css" />
<link rel="stylesheet" type="text/css"
	href="../assets/lineicons/style.css">
<link href="../assets/css/hometask.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link rel="stylesheet" type="text/css"
	href="../assets/gridster/dist/jquery.gridster.css">
<link href="../assets/css/style.css" rel="stylesheet">
<link href="../assets/css/style-responsive.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css/to-do.css">




<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<section id="container">
		<!--header start-->
		<%@include file="view/header.jsp"%>
		<!--header end-->

		<!--modal start-->
		<%@include file="view/modal/deactivate_account_modal.jsp"%>
		<%@include file="view/modal/add_group_modal.jsp"%>
		<!--modal end-->

		<!--sidebar start-->
		<%@include file="view/main_side_menu.jsp"%>
		<!--sidebar end-->

		<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
		<!--main content start-->
		<div id="editGroupModals">
		</div>
		<section id="main-content">
			<section class="wrapper">

				<div class="row">
					<div class="col-lg-9 main-chart">

						<%@include file="view/tasks.jsp"%>
						
					</div>
					<!-- /col-lg-9 END SECTION MIDDLE -->
	

					<!--sidebar start-->
					<%@include file="view/modal/add_member_modal.jsp"%>
					<%@include file="view/right_sidebar.jsp"%>
					<!--sidebar end-->

				</div>
				<! --/row -->
			</section>
		</section>

		<!--main content end-->
	</section>


	<script type="text/javascript" src="view/modal/js/script-common.js"></script>
	
	<!-- js placed at the end of the document so the pages load faster -->
	<script src="../assets/js/jquery.js"></script>
	<script src="../assets/js/jquery-1.8.3.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script class="include" type="text/javascript"
		src="../assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="../assets/js/jquery.scrollTo.min.js"></script>
	<script src="../assets/js/jquery.nicescroll.js" type="text/javascript"></script>
	<script src="../assets/js/jquery.sparkline.js"></script>
	 <script src="../js/bootbox.min.js"></script>


	<!--common script for all pages-->
	<script src="../assets/js/common-scripts.js"></script>

	<script type="text/javascript"
		src="../assets/js/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="../assets/js/gritter-conf.js"></script>

	<!--script for this page-->
	<script src="../assets/js/sparkline-chart.js"></script>
	<script src="../assets/js/zabuto_calendar.js"></script>

	<script type="application/javascript">
        
    </script>
	<script type="text/javascript" src="../assets/gridster/dist/jquery.gridster.js"></script>
		
	<script src="../assets/js/chart-master/Chart.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script type="text/javascript" src="../assets/gridster/dist/jquery.gridster.js"></script>
	<script src="../assets/autosize-master/dist/autosize.min.js" type="text/javascript"></script>
		
	
	<script src="../assets/js/tasks.js" type="text/javascript"></script>
	
	<script src="view/js/init.js"></script>
	
	<script src="view/modal/js/deactivate_account_modal.js"></script>
	<script type="text/javascript" src="view/modal/js/tasks.js"></script>
	<script src="view/modal/js/add_member_modal.js"></script>
	<script src="view/modal/js/add_group_modal.js"></script>
	 

	
	 <script src="../js/sha512.js"></script>
	
	 
</body>
</html>

