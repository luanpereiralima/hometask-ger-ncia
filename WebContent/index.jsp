﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="" />
<meta name="author" content="" />
<!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
<!-- Favicon Icon -->
<link rel="icon" href="assets2/img/favicon.ico" />
<title>Home Task</title>
<!-- BOOTSTRAP CORE CSS -->
<link href="assets2/css/bootstrap.css" rel="stylesheet" />
<!-- ION ICONS STYLES -->
<link href="assets2/css/ionicons.css" rel="stylesheet" />
<!-- FONT AWESOME ICONS STYLES -->
<link href="assets2/css/font-awesome.css" rel="stylesheet" />
<!-- CUSTOM CSS -->
<link href="assets2/css/style.css" rel="stylesheet" />
<!-- IE10 viewport hack  -->
<script src="assets2/js/ie-10-view-port.js"></script>
<!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!-- HEADER SECTION START-->
	<header id="header">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo-wrapper">
					<!-- <img src="assets/img/logo.png" alt="" /> -->
					<i class="fa fa-home"> </i> HOMETASK
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
					<div class="menu-links scroll-me">
						<a href="#header"> <i class="ion-ios-home-outline"></i>
						</a> <a href="#about"> <i class="ion-ios-camera-outline"></i>
						</a> <a href="#clients"> <i class="ion-ios-grid-view-outline"></i>
						</a> <a href="#contact"> <i class="ion-ios-chatboxes-outline"></i>
						</a>


					</div>
				</div>

			</div>


		</div>
	</header>
	<!-- HEADER SECTION END-->
	<!--HOME SECTION START  -->
	<div id="home">
		<div class="overlay">
			<div class="container">
				<div class="row scroll-me">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<h1>Home Task</h1>
						<h4>Para famílias e pessoas que dividem moradia as quais
							necessitam gerenciar suas atividades e obrigações domésticas. O
							Home Task é uma ferramenta que auxilia o gerenciamento de
							atividades e obrigações domésticas. De modo alternativo ao uso de
							planilhas eletrônicas, nosso produto funciona de forma flexível,
							notificando os usuários com lembretes e alertas e modificando os
							planejamentos caso necessário.</h4>
						<a href="signup.jsp" class="btn btn-custom btn-two">Realizar
							Cadastro</a> <a href="login.jsp" class="btn btn-custom btn-two">Realizar
							Login</a>
					</div>
				</div>


			</div>
		</div>

	</div>
	<!--HOME SECTION END  -->
	<!-- ABOUT SECTION START-->
	<section id="about">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<h2 class="head-line">Porque usar o Home Task</h2>
					<p>O Home Task funciona nos navegadores Mozilla Firefox,
						Google Chrome e Safari, além de versões mobile para Android,
						Windows Phone e iOS.</p>
					<div class="row text-center">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="row pad-bottom">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="icon-wrapper">
										<i class="ion-clipboard"></i>
									</div>
									<h4>Organize Tarefas</h4>
									Organize suas tarefas dómesticas de forma simples e amigável.

								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="icon-wrapper">
										<i class="ion-ios-alarm-outline"></i>
									</div>
									<h4>Crie Lembretes</h4>
									Nunca mais esqueça de realizar uma tarefa.


								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="icon-wrapper">
										<i class="ion-ios-people"></i>
									</div>
									<h4>Compartilhe Atividades</h4>
									Atividades podem ser compartilhadas com outras pessoas.
								</div>
							</div>

						</div>

					</div>

				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
					<img src="assets/img/phone.png" class="img-side" alt="" />
				</div>
			</div>
		</div>
	</section>
	<!-- ABOUT SECTION END-->
	<!-- CLIENTS SECTION START-->
	<section id="clients">
		<div class="overlay">
			<div class="container">
				<div class="row text-center">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"></div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  client-cover">
						<div id="carousel-example" class="carousel slide"
							data-ride="carousel">

							<div class="carousel-inner">
								<div class="item active">
									<div class="testimonial-section">Pellentesque posuere
										malesuada venenatis. Donec laoreet dapibus nulla, vitae
										feugiat metus aliquet sed.</div>
									<div class="testimonial-section-name">
										<img src="admin/sandro.jpg" alt="" class="img-circle" /> -
										Alexsandro Oliveira
									</div>

								</div>
								<div class="item">
									<div class="testimonial-section">Pellentesque posuere
										malesuada venenatis. Donec laoreet dapibus nulla, vitae
										feugiat metus aliquet sed.</div>
									<div class="testimonial-section-name">
										<img src="admin/mauro.jpg" alt="" class="img-circle" /> -
										Mauro Roberto
									</div>

								</div>
								<div class="item">
									<div class="testimonial-section">Pellentesque posuere
										malesuada venenatis. Donec laoreet dapibus nulla, vitae
										feugiat metus aliquet sed.</div>
									<div class="testimonial-section-name">
										<img src="admin/felipe.jpg" alt="" class="img-circle" /> -
										Felipe Neves
									</div>

								</div>
								<!--INDICATORS-->
								<ol class="carousel-indicators carousel-indicators-set">
									<li data-target="#carousel-example" data-slide-to="0"
										class="active"></li>
									<li data-target="#carousel-example" data-slide-to="1"></li>
									<li data-target="#carousel-example" data-slide-to="2"></li>
								</ol>

							</div>

						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- CLIENTS SECTION END-->
	<!-- FEATURES SECTION START-->
	<section id="features"></section>

	<footer>
		<div class="container">
			<div class="row text-center">
				<div class="col-lg-12 col-md-12 col-sm-12">
					&copy; 2015 HomeTask.com.br | by <a
						href="http://www.designbootstrap.com/" target="_blank"> Home
						Task </a>
				</div>

			</div>
		</div>
	</footer>
	<!-- FOOTER SECTION END-->

	<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
	<!-- CORE JQUERY  SCRIPTS -->
	<script src="assets2/js/jquery-1.11.1.js"></script>
	<!-- BOOTSTRAP SCRIPTS  -->
	<script src="assets2/js/bootstrap.js"></script>
	<!-- SCROLLING SCRIPTS PLUGIN  -->
	<script src="assets2/js/jquery.easing.min.js"></script>
	<!-- CUSTOM SCRIPTS   -->
	<script src="assets2/js/custom.js"></script>
</body>
</html>
